import logo from './logo.png';
import './App.css';
import { getByLabelText } from '@testing-library/react';

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <p>
          Welcome to<br />
          <code> Crystal Delta</code><br />
          Banking
          </p>
        <div className="my div">
          <form className="one">
            <h4>login to your account</h4>
            <label>Customer ID</label><input type="text" placeholder=" Customer ID"></input><br />
            <label>password</label><input type="password" placeholder=" password"></input><br />
            <button type="submit">Login</button>
          </form>
        </div>
      </header>
    </div>
  );
}

export default App;
